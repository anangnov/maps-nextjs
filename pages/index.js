import Image from 'next/image'
import GoogleMapReact from "google-map-react";
import "bootstrap/dist/css/bootstrap.min.css";
import { useState } from 'react';

const MapMarker = ({ text }) => (
  <div className="w-full">
    <Image alt="marker" src="/icons8-place-marker-100.png" width="40" height="40"/> 
    <p style={{ width: '100px'}}>{text}</p>
  </div>
);

export async function getServerSideProps() {
  const res = await fetch(`http://203.194.112.136:3001/api/data-json`)
  const data = await res.json()

  return { props: { data } }
}

export default function Home({ data }) {
  const defaultProps = {
    center: {
      lat: -6.214435,
      lng: 106.844967,
    },
    zoom: 11,
  };
  const [propMaps, setPropMaps] = useState({
    center: {
      lat: -6.214435,
      lng: 106.844967,
    },
    zoom: 11,
  })

  function showMarker(param) {
    setPropMaps({
      center: {
        lat: param.lat,
        lng: param.lng,
      },
      zoom: 14,
    })
  }
  
  return (
    <div>
      <div className="d-flex">
        <div className="pt-4 px-4 overflow-scroll" style={{ width: '30%', height: '100vh' }}>
          {
            data.data.map((val, i) => {
              return (
                <div key={i} onClick={() => showMarker(val.points)} style={{ cursor: 'pointer' }} className="d-flex justify-content-between pt-3 pb-4 border-bottom">
                  <div className="">
                    <p style={{ fontSize: '13px' }}>{val.title.toUpperCase()}</p>
                    <p className="mb-1" style={{ fontWeight: '500' }}>{val.name}</p>
                    <p style={{ fontSize: '12px' }}><Image alt="marker" src="/icons8-place-marker-64.png" width={14} height={14} /> {val.city}</p>
                    <div className="d-flex gap-2">
                      {
                        val.tag.map(val => {
                          return <span className="badge bg-info text-white">{val}</span>
                        })
                      }
                    </div>
                  </div>
                  <div>
                    <Image alt="icon" src="/icons8-nl-logo-100.png" width={30} height={30} />
                  </div>
                </div>
              )
            })
          }
        </div>
        <div style={{ height: "100vh", width: "70%" }}>
          <GoogleMapReact
            bootstrapURLKeys={{ key: "AIzaSyAlqVN_Y9I6JGPrCIgA5LR0iytHX1hIRaY" }}
            defaultCenter={defaultProps.center}
            defaultZoom={defaultProps.zoom}
            zoom={propMaps.zoom}
            center={propMaps.center}
          >
            { 
              data.data.map((val, i) => {
                return(
                  <MapMarker 
                    key={i}
                    text={val.name} 
                    lat={val.points.lat} 
                    lng={val.points.lng}
                  />
                )
              })
            }
          </GoogleMapReact>
        </div>
      </div>
    </div>
  );
}